#!/bin/bash
if [ -f /var/log/installer/lsb-release ]; then
    installdate=`stat -c %y /var/log/installer/lsb-release | \
    sed 's/^\([0-9\-]*\).*/\1/'`
    let difference=(`date +%s`-`date +%s -d ${installdate}`)/86400
    echo -e \
    "\e[0m\e[1;37;40m${difference}\e[0m days since last install" \
    "(`date +%D -d ${installdate}`)"
else
    echo 'Install log missing: (/var/log/installer/lsb-release)'
fi
