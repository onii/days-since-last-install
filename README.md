# Days since last install

A simple bash script to count the number of days since you installed your operating system.

Sample output:

**149** days since last install (11/12/18)
